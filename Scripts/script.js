    $('#carousel-example-generic').on('slid.bs.carousel', function () {
        $(".carousel-item.active:nth-child(" + ($(".carousel-inner .carousel-item").length -1) + ") + .carousel-item").insertBefore($(".carousel-item:first-child"));
        $(".carousel-item.active:last-child").insertBefore($(".carousel-item:first-child"));
    });  

$(".owl-carousel").on('initialized.owl.carousel changed.owl.carousel refreshed.owl.carousel', function (event) {
    if (!event.namespace) return;
    var carousel = event.relatedTarget,
        element = event.target,
        current = carousel.current();
    $('.owl-next', element).toggleClass('disabled', current === carousel.maximum());
    $('.owl-prev', element).toggleClass('disabled', current === carousel.minimum());
  });;